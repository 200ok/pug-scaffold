#!/bin/bash
# Use set -ex to debug
set -e

CLEAN=true
INSTALL=true
COUNTDEPS=true
PERFCHECK=true
BIG=true

case "$(uname -s)" in
  CYGWIN*)    OS="cygwin" ;;
  Darwin*)    OS="osx" ;;
  Linux*)     OS="linux" ;;
  MINGW32*)   OS="windows" ;;
  *)          echo "OS not detected successfully" ;;
esac
# check for WSL (bash-on-ubuntu-on-windows)
if [[ $OS = "linux" ]]; then
  if $(grep --silent 'Microsoft' /proc/sys/kernel/osrelease); then
    OS="wsl"
  fi
fi

echo "Results:" > count.txt

for i in "$@"; do
    case $i in

	    --clean)
	    CLEAN=true
	    ;;
	    --noclean)
	    CLEAN=false
	    ;;

	    --install)
	    INSTALL=true
	    ;;
	    --noinstall)
	    INSTALL=false
	    ;;

	    --countdeps)
	    COUNTDEPS=true
	    ;;
	    --nocountdeps)
	    COUNTDEPS=false
	    ;;

	    --perfcheck)
	    PERFCHECK=true
	    ;;
	    --noperfcheck)
	    PERFCHECK=false
	    ;;

	    --big)
	    BIG=true
	    ;;
	    --small)
	    BIG=false
	    ;;

        *)
        echo "Flag $i not recognised"
        ;;
    esac
done

countnpm () {
	start=`date +%s`

	if [[ "$1" == 'shellsimple' ]];then
		cd shell
	else
		cd "$1"
	fi

	pwd

	echo "OS,Scaffold,Clean,Install Duration,Source File Count,NPM Dependency Count,NPM Folder Size,NPM File Count,Dev Task Duration,Total Command Duration" >> ../count.csv

	echo "-------------------" >> ../count.txt
	echo "Starting $1 test"
	echo "$1:" >> ../count.txt

	echo -n "$OS," >> ../count.csv
	echo -n "$1," >> ../count.csv

	if [ $CLEAN = true ];then
		echo "Cleaning node_modules for $1"
		rm -rf node_modules
		echo -n "cleantrue," >> ../count.csv
	else
		echo -n "cleanfalse," >> ../count.csv
	fi

	if [ $INSTALL = true ];then
		echo "Running npm install for $1"
		echo "npm install time: " >> ../count.txt
		NPMDURATION=$({ time npm install ; } 2>&1 | grep "real\s" | awk '{print $2}')
		echo $NPMDURATION >> ../count.txt
		echo -n $NPMDURATION >> ../count.csv
		echo -n ',' >> ../count.csv
	else
		echo 'false' >> ../count.txt
		echo -n 'false' >> ../count.csv
		echo -n ',' >> ../count.csv
	fi

	if [ $BIG = true ];then
		echo "Setting up multiple file test"
		echo "Test run on 100 of each file type" >> ../count.txt
		echo -n '100,' >> ../count.csv
		for n in {001..100}; do
		    cp source/index.pug source/index-$n.pug
		    cp source/image/test-image.png source/image/test-image-$n.png
		    cp source/style/scaffold.scss source/style/scaffold-$n.scss
		    cp source/script/scaffold.js source/script/scaffold-$n.js
		    echo "function echo$n() { console.log('$n') }" >> source/script/scaffold-$n.js
		done
	else
		echo "Test run on a single example of each file type" >> ../count.txt
		echo -n '1,' >> ../count.csv
	fi

	if [ $COUNTDEPS = true ];then
		echo "Counting dependencies for $1"
		NPMDEPS=$(npm ls | wc -l)
		NPMSIZE=$(du -sh node_modules | cut -f1)
		NPMFILECOUNT=$(find node_modules -type f | wc -l )
	else
		echo "Skipping dependency count"
		NPMDEPS=null
		NPMSIZE=null
		NPMFILECOUNT=null
	fi
	echo "npm deps  	$NPMDEPS" >> ../count.txt
	echo "npm size  	$NPMSIZE" >> ../count.txt
	echo "npm files 	$NPMFILECOUNT" >> ../count.txt

	echo -n "$NPMDEPS,$NPMSIZE,$NPMFILECOUNT," >> ../count.csv

	if [ $PERFCHECK = true ];then

		if [[ $OS = "osx" ]]; then
		    perfcheckstart=$(date +%s)
		else
		    perfcheckstart=$(date +%s%N)
		fi

		echo "dev task time: " >> ../count.txt
		for scaffold in "$@"; do
		    case $scaffold in
			    "grunt")
				TASKTIME=$({ time ./node_modules/.bin/grunt dev ; } 2>&1 | grep "real\s" | awk '{print $2}')
			    ;;

			    "gulp")
				TASKTIME=$({ time ./node_modules/.bin/gulp dev ; } 2>&1 | grep "real\s" | awk '{print $2}')
			    ;;

			    "npm")
				TASKTIME=$({ time npm run dev ; } 2>&1 | grep "real\s" | awk '{print $2}')
			    ;;

			    "make")
				TASKTIME=$({ time make dev ; } 2>&1 | grep "real\s" | awk '{print $2}')
			    ;;

			    "shell")
				TASKTIME=$({ time ./run.sh dev ; } 2>&1 | grep "real\s" | awk '{print $2}')
			    ;;

			    "shellsimple")
				TASKTIME=$({ time ./run-simple.sh dev ; } 2>&1 | grep "real\s" | awk '{print $2}')
			    ;;

		        *)
		        echo "Scaffold $scaffold not recognised"
		        ;;
		    esac
		done
		echo "$TASKTIME" >> ../count.txt
		echo -n "$TASKTIME," >> ../count.csv

		# if [[ $OS = "osx" ]]; then
		#     perfcheckend=$(date +%s)
		#     # Calculate execution time in seconds
		#     perfcheckexectime=$((end-start))
		#     # Note that it's pretty vague on OSX.
		#     echo "Done (~${perfcheckexectime}s)." >> ../count.txt
		#     echo "Note sub-second measurement not available because OSX is stuck on Bash 3. *shakes fist*" >> ../count.txt
		# else
		#     perfcheckend=`date +%s%N`
		#     # Calculate execution time in ms
		#     perfcheckexectime=$(((end-start)/1000000))
		#     echo "Dev (${perfcheckexectime} ms)" >> ../count.txt
		# fi

	fi

	if [ $BIG = true ];then
		echo "Clean up multiple file test"
		rm source/index-*.pug
		rm source/image/test-image-*.png
		rm source/style/scaffold-*.scss
		rm source/script/scaffold-*.js
	fi

	end=`date +%s`
	echo "Full task: ($((end-start))s)" >> ../count.txt
	echo -n "$((end-start))," >> ../count.csv
	echo "Done.">> ../count.csv

	cd ..
}

countnpm grunt
countnpm gulp
countnpm npm
countnpm shell
# countnpm shellsimple
countnpm make

cat count.txt
cat count.csv
