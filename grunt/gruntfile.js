module.exports = function (grunt) {

  var sourceDir = 'source/';
  var outputDir = 'output/';

  var prodBanner = '/*! <%= pkg.name %> <%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd") %> */';
  var devBanner =  '/*! <%= pkg.name %> non-production version <%= grunt.template.today("yyyy-mm-dd") %> */';

  // JIT avoids the need to specify every plugin
  // and speeds things up quite a bit
  require('jit-grunt')(grunt, {
    'usebanner':  'grunt-banner'
  });

  // Gives a nice readout on how long things are taking
  require('time-grunt')(grunt);
 
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // HTML
    // Convert all non-underscored .pug files
    pug: {
      dev: {
        options: {
          data: {
            debug: false
          },
          pretty: true
        },
        files: [{
          cwd: sourceDir,
          src: ['**/*.pug','!**/*_*.pug'],
          dest: outputDir,
          expand: true,
          ext: '.html'
        }]
      },
      prod: {
        options: {
          data: {
            debug: false
          },
          pretty: false
        },
        files: [{
          cwd: sourceDir,
          src: ['**/*.pug','!**/*_*.pug'],
          dest: outputDir,
          expand: true,
          ext: '.html'
        }]
      }
    },

    // CSS
    sass: {
      dev: {
        options: {
          outputStyle: 'expanded',
          sourceComments: true,
          precision: 5
        },
        files: [{
          expand: true,
          cwd: sourceDir+'style',
          src: ['*.scss'],
          dest: outputDir+'style/',
          ext: '.css'
        }]
      },
      prod: {
        options: {
          outputStyle: 'compressed',
          sourceComments: false,
          precision: 5
        },
        files: [{
          expand: true,
          cwd: sourceDir+'style',
          src: ['*.scss'],
          dest: outputDir+'style/',
          ext: '.css'
        }]
      },
    },

    // JavaScript
    uglify: {
      dev: {
        options: {
          preserveComments: true,
          mangle: false,
          compress: false,
          beautify: true
        },
        files: [{
          src: sourceDir+'script/*.js',
          dest: outputDir+'script/scaffold.js'
        }]
      },
      prod: {
        options: {
          preserveComments: true,
          mangle: true
        },
        files: [{
          src: sourceDir+'script/*.js',
          dest: outputDir+'script/scaffold.js'
        }]
      }
    },

    // Other files, eg. images
    copy: {
      dist: {
        files: [
          { 
            expand: true, 
            flatten: true, 
            src: ['source/image/*'], 
            dest: 'output/image/', 
            filter: 'isFile'
          }
        ]
      }
    },

    // Clean things up
    clean: [outputDir, 'temp/'],

    watch: {
      pug: {
        files: [sourceDir+'/**/*.pug'],
        tasks: ['pug:dev'],
      },
      scripts: {
        files: [sourceDir+'/**/*.js'],
        tasks: ['uglify', 'usebanner:devjs'],
      },
      sass: {
        files: [sourceDir+'/**/*.scss'],
        tasks: ['sass:dev', 'usebanner:devcss'],
      },
      copy: {
        files: [sourceDir+'/image/**/*.*'],
        tasks: ['copy'],
      }
    },

    usebanner: {
      devcss: {
        options: {
          position: 'top',
          banner: devBanner
        },
        files: { src: [outputDir+'/**/*.css']}
      },
      devjs: {
        options: {
          position: 'top',
          banner: devBanner
        },
        files: { src: [outputDir+'/**/*.js']}
      },
      prod: {
        options: {
          position: 'top',
          banner: prodBanner
        },
        files: { src: [
          outputDir+'/**/*.css',
          outputDir+'/**/*.js'
        ]}
      }
    }

  });

  grunt.registerTask('default', ['prod']);
  grunt.registerTask('prod', ['clean','pug:prod','uglify:prod','sass:prod','copy','usebanner:prod']);
  grunt.registerTask('dev', ['clean','pug:dev','uglify:dev','sass:dev','copy','usebanner:devcss','usebanner:devjs']);
  grunt.registerTask('watch', ['dev','watch']);

}