#!/bin/bash
# Use set -ex to debug
set -e

SOURCEDIR="./source"
OUTPUTDIR="./output"
NPMBIN="./node_modules/.bin"

setup () {
    npm install
}

clean () {
    echo "Cleaning built assets"
    rm -rf ${OUTPUTDIR}
}

build () {

    if [ "$1" == "dev" ];then
        PUGFORMAT="--pretty"
        SCSSFORMAT="expanded"
    elif [ "$1" == "prod" ];then
        PUGFORMAT=""
        SCSSFORMAT="compressed"
    else
        echo "Build target $1 not recognised."
        exit 1
    fi

    echo "Building $1 files"
    ${NPMBIN}/pug ${SOURCEDIR}/[!_]*.pug --out ${OUTPUTDIR} ${PUGFORMAT}
    ${NPMBIN}/node-sass ${SOURCEDIR}/style/ --output ${OUTPUTDIR}/style/ --output-style ${SCSSFORMAT}

}

copy () {
    echo "Copying built assets"
    mkdir ${OUTPUTDIR}/image
    cp ${SOURCEDIR}/image/* ${OUTPUTDIR}/image/    
}

if [ $# -eq 0 ]; then
    echo "No command provided. Expected setup, clean, copy, dev or prod."
    exit 1
else

    # Read options
    for i in "$@"; do
        case $i in

            "setup")
                setup
            shift;;

            "clean")
                clean
            shift;;

            "copy")
                copy
            shift;;

            "dev")
                clean
                build dev
                copy
            shift;;

            "prod")
                clean
                build prod
                copy
            shift;;

            *)
            echo "Command \"$i\" not recognised.";;
        esac
    done

    echo "Done."
    exit 0

fi

