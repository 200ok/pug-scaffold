#!/bin/bash
# Use set -ex to debug
set -e

case "$(uname -s)" in
  CYGWIN*)    OS="cygwin" ;;
  Darwin*)    OS="osx" ;;
  Linux*)     OS="linux" ;;
  MINGW32*)   OS="windows" ;;
  *)          echo "OS not detected successfully" ;;
esac

if [[ $OS = "osx" ]]; then
    start=$(date +%s)
else
    start=$(date +%s%N)
fi

# Usage: parseJsonWithSed 'key' 'path/filename'
parseJsonWithSed () {
    echo "$( sed -n "s/.*\"$1\": \"\(.*\)\",/\1/p" "$2" )"
}

SOURCEDIR="./source"
OUTPUTDIR="./output"
NPMBIN="./node_modules/.bin"

banner () {

    NAME=$(parseJsonWithSed 'name' './package.json')
    VERSION=$(parseJsonWithSed 'version' './package.json')
    DATE=$(date +%Y-%m-%d" "%H:%M:%S)
    BANNER="/*! $NAME $VERSION generated at $DATE */";

    echo -e "Prepending comment banner to .css and .js files"
    find $OUTPUTDIR -type f \( -name "*.css" -or -name "*.js" \)|while IFS="" read -r filename; do
        echo -e "Adding banner to" "$filename"
        echo -e "$BANNER\n$(cat "$filename")" > "$filename"
    done

}

setup () {
    npm install
}

clean () {
    echo "Cleaning built assets"
    if [[ $OUTPUTDIR == "./"* ]];then
        rm -rf ${OUTPUTDIR}
    else
        echo "ABORTING: \$OUTOUTDIR must be a subdirectory specified with leading ./ eg. \"./output\""
        exit 1
    fi
}

copy () {
    echo "Copying built assets"
    mkdir ${OUTPUTDIR}/image
    cp ${SOURCEDIR}/image/* ${OUTPUTDIR}/image/    
}

build-dev () {
    echo "Building development assets"
    ${NPMBIN}/pug ${SOURCEDIR}/[!_]*.pug --out ${OUTPUTDIR} --pretty
    ${NPMBIN}/node-sass ${SOURCEDIR}/style/ --output ${OUTPUTDIR}/style/ --output-style expanded
    mkdir -p ${OUTPUTDIR}/script
    ${NPMBIN}/uglifyjs ${SOURCEDIR}/script/*.js > ${OUTPUTDIR}/script/scaffold.min.js
}

build-prod () {
    echo "Building production assets"
    ${NPMBIN}/pug ${SOURCEDIR}/[!_]*.pug --out ${OUTPUTDIR}
    ${NPMBIN}/node-sass ${SOURCEDIR}/style/ --output ${OUTPUTDIR}/style/ --output-style compressed
    mkdir -p ${OUTPUTDIR}/script
    ${NPMBIN}/uglifyjs ${SOURCEDIR}/script/*.js > ${OUTPUTDIR}/script/scaffold.min.js
}

watchtask () {
    echo "Building and watching for changes."
    # This will run once on initial load so no need to call the other task first
    ${NPMBIN}/watch "./run.sh dev" ${SOURCEDIR}
}

if [ $# -eq 0 ]; then
    echo "No command provided. Expected setup, dev or prod."
    exit 1
else

    # Read options
    for i in "$@"; do
        case $i in

            "setup")
                setup
            shift;;

            "clean")
                clean
            shift;;

            "copy")
                copy
            shift;;

            "dev")
                clean
                build-dev
                copy
            shift;;

            "watch")
                watchtask
            shift;;

            "prod")
                clean
                build-prod
                copy
                banner
            shift;;

            *)
            echo "Command \"$i\" not recognised."
            exit 1
            ;;
        esac
    done

    if [[ $OS = "osx" ]]; then

        end=$(date +%s)
        exectime=$((end-start))
        echo "Done (~${exectime}s)."
        echo "Note sub-second measurement not available because OSX is stuck on Bash 3. *shakes fist*"

    else

        end=$(date +%s%N)
        # Calculate execution time in ms
        exectime=$(((end-start)/1000000))
        # Insert . to express as seconds
        echo "Done (${exectime:0:-3}.${exectime:-3}s)."

    fi

fi

