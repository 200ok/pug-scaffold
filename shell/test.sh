#!/bin/bash
# Use set -ex to debug
set -e

# echo $( sed -n 's/.*"version": "\(.*\)",/\1/p' ./package.json )

parseJsonWithSed () {
	echo "$( sed -n "s/.*\"$1\": \"\(.*\)\",/\1/p" "$2" )"
}
# parseJsonWithPython () {}

# echo $( sed -n 's/.*"MYSQL_USER": "\(.*\)",/\1/p' /var/lib/credentials.json )

echo 
VERSION=$(parseJsonWithSed 'version' './package.json')
echo "Abstracted: $VERSION"


NAME=$(python -c "import json,sys;obj=json.load(sys.stdin);print obj['name'];" < package.json)
VERSION=$(python -c "import json,sys;obj=json.load(sys.stdin);print obj['version'];" < package.json) 


# OUTPUTDIR="asfasfd./output"
# # OUTPUTDIR="/"

# if [[ $OUTPUTDIR == "./"* ]];then
# 	echo "do the thing"
# else
# 	echo "To avoid data loss, \$OUTOUTDIR must be a subdirectory specified with leading ./ eg. \"./output\""
# fi

# # if [ -d ~/folder ]



# if [[ -z "$OUTPUTDIR" || "$OUTPUTDIR" = "/" || "$OUTPUTDIR" = "~" ]];then
# 	echo "\$OUTPUTDIR is unset, or set to a value that would cause serious data loss (due to the clean task). Aborting."
# 	exit 1
# else
# 	echo "\$OUTPUTDIR is not empty"
#     # rm -rf ${OUTPUTDIR}
# fi



# DATE=$(date +%Y-%m-%d" "%H:%M:%S)
# NAME=$(cat package.json | python -c "import json,sys;obj=json.load(sys.stdin);print obj['name'];")
# VERSION=$(cat package.json | python -c "import json,sys;obj=json.load(sys.stdin);print obj['version'];")
 
# BANNER="/*! $NAME $VERSION generated at $DATE */";


# echo -e "Banner: $BANNER"
# find $OUTPUTDIR -type f \( -name "*.css" -or -name "*.js" \)|while IFS="" read -r filename; do
#     echo -e "Adding banner to "$filename""
#     echo -e "$BANNER\n$(cat "$filename")" > "$filename"
# done
# echo -e "Banners done."

# # doesn't work on mac:
# cat package.json | grep -Po '(?<="name": ")[^"]*'
# cat package.json | grep -Po '(?<="version": ")[^"]*'


# declare -a values # declare the array
# # Read each line and use regex parsing (with Bash's `=~` operator) to extract the value.
# while read -r line; do
#   # Extract the value from between the double quotes and add it to the array.
#   [[ $line =~ :[[:blank:]]+\"(.*)\" ]] && values+=( "${BASH_REMATCH[1]}" )
# done < package.json
# declare -p values # print the array
# echo ${values[0]}
# echo ${values[1]}
