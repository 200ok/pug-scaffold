#!/bin/bash
# Use set -ex to debug
set -e

# This file is just to illustrate how simple this bash script could be 
# without abstraction. There is a lot of path repetition, but it's
# still pretty readable.

if [ $# -eq 0 ]; then
    echo "No command provided. Expected setup, dev, watch or prod."
    exit 1
else
    for i in "$@"; do
        case $i in

            "setup")
                npm install
            shift;;

            "dev")
                rm -rf ./output
                ./node_modules/.bin/pug ./source/[!_]*.pug --out ./output --pretty
                ./node_modules/.bin/node-sass ./source/style/ --output ./output/style/ --output-style expanded
                mkdir ./output/image
                cp ./source/image/* ./output/image/    
            shift;;

            "watch")
                # This will run once on initial load so no need to call the other task first
                ${NPMBIN}/watch "./run.sh dev" ${SOURCEDIR}
            shift;;

            "prod")
                rm -rf ./output
                ./node_modules/.bin/pug ./source/[!_]*.pug --out ./output
                ./node_modules/.bin/node-sass ./source/style/ --output ./output/style/ --output-style compressed
                mkdir ./output/image
                cp ./source/image/* ./output/image/    
            shift;;

            *)
            echo "Command \"$i\" not recognised.";;
        esac
    done
    exit 0
fi
