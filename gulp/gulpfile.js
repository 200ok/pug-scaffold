'use strict';
var del = require('del');
var gulp = require('gulp');
var info = require('./package.json')
var insert = require('gulp-insert');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

// this won't be required after Gulp 4 comes out
var gulpsync = require('gulp-sync')(gulp);

// used with insert to insert banner comment in CSS and JS
// takes values from package.json
var now = new Date().toISOString().slice(0,19).replace(/[-T:]/g,"");
var banner = `/*! ${info.name} v${info.version} generated ${now} */`;

var paths = {
  destination: 'output',
  markup: {
    src: ['source/*.pug', '!source/_*.pug'],
    dest: 'output'
  },
  styles: {
    src: 'source/style/*.scss',
    dest: 'output/style'
  },
  scripts: {
    src: 'source/script/**/*.js',
    dest: 'output/script'
  },
  images: {
    src: 'source/image/**/*',
    dest: 'output/image'
  }
};


// Note that clean does not use the pipe
gulp.task('clean', function() {
  return del([paths.destination]);
}); 

gulp.task('clean-copy', function() {
  return del([paths.images.dest]);
}); 

gulp.task('copy', ['clean-copy'], function() {
  return gulp.src(paths.images.src)
    .pipe(gulp.dest(paths.images.dest));
});

gulp.task('pug:dev', function() {
  return gulp.src(paths.markup.src)
    .pipe(pug({
      'pretty':true
    }))
    .pipe(gulp.dest(paths.markup.dest))
});
gulp.task('pug:prod', function() {
  return gulp.src(paths.markup.src)
    .pipe(pug())
    .pipe(gulp.dest(paths.markup.dest))
});

gulp.task('sass:dev', function () {
  return gulp.src(paths.styles.src)
    .pipe(sass({
      'outputStyle': 'expanded'
    }).on('error', sass.logError))
    .pipe(insert.prepend(banner))
    .pipe(gulp.dest(paths.styles.dest));
});
gulp.task('sass:prod', function () {
  return gulp.src(paths.styles.src)
    .pipe(sass({
      'outputStyle': 'compressed'
    }).on('error', sass.logError))
    .pipe(insert.prepend(banner))
    .pipe(gulp.dest(paths.styles.dest));
});

gulp.task('uglify:dev', function () {
  return gulp.src(paths.scripts.src)
    .pipe(uglify({
      'mangle': false,
      'compress': false,
      'output': { beautify: true },
      'preserveComments': 'license'
    }))
    .pipe(insert.prepend(banner))
    .pipe(gulp.dest(paths.scripts.dest));
});
gulp.task('uglify:prod', function () {
  return gulp.src(paths.scripts.src)
    .pipe(uglify({
      'preserveComments': 'license'
    }))
    .pipe(insert.prepend(banner))
    .pipe(gulp.dest(paths.scripts.dest));
});

gulp.task('devwatch', function() {
  gulp.watch(paths.markup.src, ['pug:dev']);
  gulp.watch(paths.styles.src, ['sass:dev']);
  gulp.watch(paths.scripts.src, ['uglify:dev']);
  gulp.watch(paths.images.src, ['copy']);
});


gulp.task('default', ['dev']);
gulp.task('build-dev', ['pug:dev','copy','sass:dev','uglify:dev']);
gulp.task('build-prod', ['pug:prod','copy','sass:prod','uglify:prod']);

// gulpsync used to ensure clean runs first
// in Gulp 4 which will support both sync and async
// gulp.series(clean, build-dev);
gulp.task('dev', gulpsync.sync(['clean', 'build-dev']));
gulp.task('watch', gulpsync.sync(['clean', 'build-dev', 'devwatch']));
gulp.task('prod', gulpsync.sync(['clean', 'build-prod']));

